#include </usr/include/lib.h>
#include "stdlib.h"
#include "stdio.h"

int main(int argc, char*  argv[])	{
	char group_sign;
	int i = 0;
	int j = 0;
	int a = 0;
	message msg_in;
	msg_in.m1_i1 = getpid();
	group_sign = _syscall(MM, GET_PROC_GROUP, &msg_in);
	printf("Process %s group is %c.\n", argv[1], group_sign);
	msg_in.m1_i1 = getpid();
	_syscall(MM, CHANGE_GROUP, &msg_in);
	msg_in.m1_i1 = getpid();
	group_sign = _syscall(MM, GET_PROC_GROUP, &msg_in);
	printf("Process %s group is %c.\n", argv[1], group_sign);
	for(i = 0; i < 30000; ++i)
		for(j = 0; j < 10000; ++j)
			a += i + j;
	return 0;
}
